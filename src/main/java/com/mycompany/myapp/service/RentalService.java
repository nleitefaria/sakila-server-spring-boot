package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.RentalDTO;

public interface RentalService 
{	
	RentalDTO findOne(Integer id);
	List<RentalDTO> findAll();
	PagginatedDTO<RentalDTO> findAllPaginated(int pageNum, int pageSize);
	void save(RentalDTO rentalDTO) throws javax.persistence.RollbackException;
}
