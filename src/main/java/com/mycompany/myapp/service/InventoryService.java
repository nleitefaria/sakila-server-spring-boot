package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.InventoryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface InventoryService
{	
	InventoryDTO findOne(Integer id);
	List<InventoryDTO> findAll();
	PagginatedDTO<InventoryDTO> findAllPaginated(int pageNum, int pageSize);
	void save(InventoryDTO inventoryDTO) throws javax.persistence.RollbackException;
}
