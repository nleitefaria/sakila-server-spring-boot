package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CategoryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface CategoryService 
{
	CategoryDTO findOne(Short id);
	List<CategoryDTO> findAll() throws javax.persistence.RollbackException;
	PagginatedDTO<CategoryDTO> findAllPaginated(int pageNum, int pageSize);
	void save(CategoryDTO categoryDTO) throws javax.persistence.RollbackException;
	CategoryDTO update(short id, CategoryDTO categoryDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
