package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface AddressService 
{
	AddressDTO findOne(Short id);
	List<AddressDTO> findAll();
	void save(AddressDTO addressDTO) throws javax.persistence.RollbackException;
	PagginatedDTO<AddressDTO> findAllPaginated(int pageNum, int pageSize);
	AddressDTO update(short id, AddressDTO addressDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
