package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface CustomerService 
{
	CustomerDTO findOne(Short id);
	List<CustomerDTO> findAll();
	PagginatedDTO<CustomerDTO> findAllPaginated(int pageNum, int pageSize);
	void save(CustomerDTO customerDTO) throws javax.persistence.RollbackException;
}
