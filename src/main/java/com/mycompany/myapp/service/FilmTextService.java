package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.FilmTextDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface FilmTextService 
{
	FilmTextDTO findOne(Short id);
	List<FilmTextDTO> findAll();
	PagginatedDTO<FilmTextDTO> findAllPaginated(int pageNum, int pageSize);
	void save(FilmTextDTO filmTextDTO) throws javax.persistence.RollbackException;
	FilmTextDTO update(short id, FilmTextDTO filmTextDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
