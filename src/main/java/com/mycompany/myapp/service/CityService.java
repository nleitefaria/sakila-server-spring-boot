package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface CityService
{
	CityDTO findOne(Short id);
	List<CityDTO> findAll();
	PagginatedDTO<CityDTO> findAllPaginated(int pageNum, int pageSize);
	void save(CityDTO cityDTO) throws javax.persistence.RollbackException;
	CityDTO update(short id, CityDTO cityDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
