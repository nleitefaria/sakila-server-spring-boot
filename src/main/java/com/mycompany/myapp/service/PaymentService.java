package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.PaymentDTO;

public interface PaymentService 
{	
	PaymentDTO findOne(Short id);
	List<PaymentDTO> findAll();
	PagginatedDTO<PaymentDTO> findAllPaginated(int pageNum, int pageSize);
	void save(PaymentDTO paymentDTO) throws javax.persistence.RollbackException;
}
