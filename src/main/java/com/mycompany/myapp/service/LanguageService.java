package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.LanguageDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface LanguageService 
{	
	LanguageDTO findOne(Short id);
	List<LanguageDTO> findAll();
	PagginatedDTO<LanguageDTO> findAllPaginated(int pageNum, int pageSize);
	void save(LanguageDTO languageDTO) throws javax.persistence.RollbackException;
	LanguageDTO update(short id, LanguageDTO languageDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
