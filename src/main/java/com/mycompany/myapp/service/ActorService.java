package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface ActorService
{
	ActorDTO findOne(Short id);
	List<ActorDTO> findAll();
	PagginatedDTO<ActorDTO> findAllPaginated(int pageNum, int pageSize);
	void save(ActorDTO actorDTO) throws javax.persistence.RollbackException; 
	ActorDTO update(short id, ActorDTO actorDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
