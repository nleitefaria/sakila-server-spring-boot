package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.StoreDTO;

public interface StoreService {
	
	StoreDTO findOne(Short id);
	List<StoreDTO> findAll();

}
