package com.mycompany.myapp.service;

import java.util.List;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface CountryService 
{
	CountryDTO findOne(Short id);
	List<CountryDTO> findAll();
	PagginatedDTO<CountryDTO> findAllPaginated(int pageNum, int pageSize);
	void save(CountryDTO countryDTO) throws javax.persistence.RollbackException;
	CountryDTO update(short id, CountryDTO countryDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
}
