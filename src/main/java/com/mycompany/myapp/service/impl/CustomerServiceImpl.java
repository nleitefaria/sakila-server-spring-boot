package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	StoreRepository storeRepository;
	
	@Transactional
	public CustomerDTO findOne(Short id)
	{
		Customer customer = customerRepository.findOne(id);
		return new CustomerDTO(customer);
	}

	@Transactional
	public List<CustomerDTO> findAll() 
	{
		List<CustomerDTO> customerDTOList = new ArrayList<CustomerDTO>();
		List<Customer> customerList = customerRepository.findAll();

		for (Customer customer : customerList) {
			CustomerDTO customerDTO = new CustomerDTO(customer);
			customerDTOList.add(customerDTO);
		}

		return customerDTOList;
	}
	
	@Transactional
	public PagginatedDTO<CustomerDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<CustomerDTO> customerDTOList = new ArrayList<CustomerDTO>();			
		Page<Customer> customerPage = customerRepository.findAll(new PageRequest(pageNum - 1, pageSize));
			
		for (Customer customer : customerPage.getContent()) {
			CustomerDTO customerDTO = new CustomerDTO(customer);
			customerDTOList.add(customerDTO);
		}

			
		PagginatedDTO<CustomerDTO> customerDTOPage = new PagginatedDTO<CustomerDTO>(customerDTOList, customerPage.getNumber(), customerPage.getNumberOfElements(), customerPage.getTotalPages(), customerPage.getTotalElements());
		return customerDTOPage;
	}
		
	@Transactional
	public void save(CustomerDTO customerDTO) throws javax.persistence.RollbackException
	{		
		Address address = addressRepository.findOne(customerDTO.getAddress().getAddressId());
		Store store = storeRepository.findOne(customerDTO.getStore().getStoreId());
		Customer customer = new Customer(address, store, customerDTO.getFirstName(), customerDTO.getLastName(), customerDTO.getEmail(), customerDTO.isActive(), customerDTO.getCreateDate(), customerDTO.getLastUpdate());		
		customerRepository.save(customer);	
	}	
}
