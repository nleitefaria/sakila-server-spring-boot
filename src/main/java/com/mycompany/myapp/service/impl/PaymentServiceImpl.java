package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.PaymentDTO;

import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.entity.Payment;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.repository.PaymentRepository;
import com.mycompany.myapp.repository.StaffRepository;
import com.mycompany.myapp.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService
{
	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	StaffRepository staffRepository;

	
	@Transactional
	public PaymentDTO findOne(Short id) 
	{
		Payment payment = paymentRepository.findOne(id);
		return new PaymentDTO(payment);
	}
	
	@Transactional
	public List<PaymentDTO> findAll() 
	{
		List<PaymentDTO> paymentDTOList = new ArrayList<PaymentDTO>();
		List<Payment> paymentList = paymentRepository.findAll();

		for (Payment payment : paymentList)
		{
			PaymentDTO paymentDTO = new PaymentDTO(payment);
			paymentDTOList.add(paymentDTO);
		}

		return paymentDTOList;
	}
	
	@Transactional
	public PagginatedDTO<PaymentDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<PaymentDTO> paymentDTOList = new ArrayList<PaymentDTO>();				
		Page<Payment> paymentPage = paymentRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Payment payment : paymentPage.getContent())
		{
			PaymentDTO paymentDTO = new PaymentDTO(payment);
			paymentDTOList.add(paymentDTO);		
		}
		
		PagginatedDTO<PaymentDTO> paymentDTOPage = new PagginatedDTO<PaymentDTO>(paymentDTOList, paymentPage.getNumber(), paymentPage.getNumberOfElements(), paymentPage.getTotalPages(), paymentPage.getTotalElements());
		return paymentDTOPage;
	}
	
	@Transactional
	public void save(PaymentDTO paymentDTO) throws javax.persistence.RollbackException
	{
		Customer customer = customerRepository.findOne(paymentDTO.getCustomer().getCustomerId());
		Staff staff = staffRepository.findOne(paymentDTO.getStaff().getStaffId());
		Payment payment = new Payment(customer, staff, paymentDTO.getAmount(), paymentDTO.getPaymentDate(), new Date());
		paymentRepository.save(payment);	
	}	

}
