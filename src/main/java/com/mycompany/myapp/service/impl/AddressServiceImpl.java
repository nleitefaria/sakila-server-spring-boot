package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.City;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.CityRepository;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService
{
	private static final Logger logger = LoggerFactory.getLogger(AddressServiceImpl.class);

	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	CountryRepository countryRepository;

	@Transactional
	public AddressDTO findOne(Short id) {
		Address address = addressRepository.findOne(id);
		return new AddressDTO(address);
	}

	@Transactional
	public List<AddressDTO> findAll() {
		List<AddressDTO> addressDTOList = new ArrayList<AddressDTO>();
		List<Address> addressList = addressRepository.findAll();

		for (Address address : addressList) {
			AddressDTO addressDTO = new AddressDTO(address);
			addressDTOList.add(addressDTO);
		}
		return addressDTOList;
	}
	
	@Transactional
	public PagginatedDTO<AddressDTO> findAllPaginated(int pageNum, int pageSize) 
	{
		List<AddressDTO> addressDTOList = new ArrayList<AddressDTO>();				
		Page<Address> addressPage = addressRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Address address : addressPage.getContent())
		{
			AddressDTO addressDTO = new AddressDTO(address);
			addressDTOList.add(addressDTO);		
		}
		
		PagginatedDTO<AddressDTO> addressDTOPage = new PagginatedDTO<AddressDTO>(addressDTOList, addressPage.getNumber(), addressPage.getNumberOfElements(), addressPage.getTotalPages(), addressPage.getTotalElements());
		return addressDTOPage;
	}
	
	@Transactional
	public void save(AddressDTO addressDTO) throws javax.persistence.RollbackException  
	{		
			City city = cityRepository.findOne(addressDTO.getCity().getCityId());
			
			if(city == null)
			{					
				Country country = countryRepository.findOne(addressDTO.getCity().getCountry().getCountryId());
				
				if(country == null)
				{
					logger.info("Creating new country");				
					country = new Country(addressDTO.getCity().getCountry().getCountry(), new Date());
					countryRepository.save(country);
				}
				
				logger.info("Creating new city");
				city = new City(country, addressDTO.getCity().getCity(), new Date());
				cityRepository.save(city);
			}
			
			Address address = new Address(city, addressDTO.getAddress(), addressDTO.getDistrict(), addressDTO.getPhone(), new Date());
			addressRepository.save(address);	
	}
	
	@Transactional
	public AddressDTO update(short id, AddressDTO addressDTO) throws javax.persistence.RollbackException   
	{		
		Address address = addressRepository.findOne(id);
		
		if(address != null)
		{
			address.setAddress(addressDTO.getAddress());
			address.setAddress2(addressDTO.getAddress2());
			
			AddressDTO ret = new AddressDTO(address);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Address address = addressRepository.findOne(id);
		
		if(address != null)
		{
			addressRepository.delete(address);
			return id;
		}
		else
		{
			return null;		
		}			
	}

}
