package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.LanguageDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Language;
import com.mycompany.myapp.repository.LanguageRepository;
import com.mycompany.myapp.service.LanguageService;

@Service
public class LanguageServiceImpl implements LanguageService
{
	@Autowired
	LanguageRepository languageRepository;

	@Transactional
	public LanguageDTO findOne(Short id) 
	{
		Language language = languageRepository.findOne(id);
		return new LanguageDTO(language);
	}
	
	@Transactional
	public List<LanguageDTO> findAll() {
		List<LanguageDTO> languageDTOList = new ArrayList<LanguageDTO>();
		List<Language> languageList = languageRepository.findAll();

		for (Language language : languageList) {
			LanguageDTO languageDTO = new LanguageDTO(language);
			languageDTOList.add(languageDTO);
		}

		return languageDTOList;
	}
	
	@Transactional
	public PagginatedDTO<LanguageDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<LanguageDTO> languageDTOList = new ArrayList<LanguageDTO>();			
		Page<Language> languagePage = languageRepository.findAll(new PageRequest(pageNum - 1, pageSize));
			
		for (Language language : languagePage.getContent()) {
			LanguageDTO languageDTO = new LanguageDTO(language);
			languageDTOList.add(languageDTO);
		}
			
		PagginatedDTO<LanguageDTO> languageDTOPage = new PagginatedDTO<LanguageDTO>(languageDTOList, languagePage.getNumber(), languagePage.getNumberOfElements(), languagePage.getTotalPages(), languagePage.getTotalElements());
		return languageDTOPage;
	}
		
	@Transactional
	public void save(LanguageDTO languageDTO) throws javax.persistence.RollbackException
	{		
		Language language = new Language(languageDTO.getName(), new Date());		
		languageRepository.save(language);	
	}
	
	@Transactional
	public LanguageDTO update(short id, LanguageDTO languageDTO) throws javax.persistence.RollbackException   
	{		
		Language language = languageRepository.findOne(id);
			
		if(language != null)
		{
			language.setName(languageDTO.getName());
			language.setLastUpdate(new Date());
			LanguageDTO ret = new LanguageDTO(language);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
		
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Language language = languageRepository.findOne(id);
			
		if(language != null)
		{
			languageRepository.delete(language);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	
}
