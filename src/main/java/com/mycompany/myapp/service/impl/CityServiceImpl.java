package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.City;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.repository.CityRepository;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.service.CityService;

@Service
public class CityServiceImpl implements CityService 
{
	private static final Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	CountryRepository countryRepository;

	@Transactional
	public CityDTO findOne(Short id) {
		City city = cityRepository.findOne(id);
		return new CityDTO(city);
	}

	@Transactional
	public List<CityDTO> findAll() {
		List<CityDTO> cityDTOList = new ArrayList<CityDTO>();
		List<City> cityList = cityRepository.findAll();

		for (City city : cityList) {
			CityDTO cityDTO = new CityDTO(city);
			cityDTOList.add(cityDTO);
		}
		return cityDTOList;
	}
	
	@Transactional
	public PagginatedDTO<CityDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<CityDTO> cityDTOList = new ArrayList<CityDTO>();				
		Page<City> cityPage = cityRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(City city : cityPage.getContent())
		{
			CityDTO cityDTO = new CityDTO(city);
			cityDTOList.add(cityDTO);		
		}
		
		PagginatedDTO<CityDTO> cityDTOPage = new PagginatedDTO<CityDTO>(cityDTOList, cityPage.getNumber(), cityPage.getNumberOfElements(), cityPage.getTotalPages(), cityPage.getTotalElements());
		return cityDTOPage;
	}
	
	@Transactional
	public void save(CityDTO cityDTO) throws javax.persistence.RollbackException
	{				
		Country country = countryRepository.findOne(cityDTO.getCountry().getCountryId());
			
		if(country == null)
		{
			logger.info("Creating new country");				
			country = new Country(cityDTO.getCountry().getCountry(), new Date());
			countryRepository.save(country);
		}
			
		logger.info("Creating new city");
		City city = new City(country,cityDTO.getCity(), new Date());
		cityRepository.save(city);		
	}
	
	@Transactional
	public CityDTO update(short id, CityDTO cityDTO) throws javax.persistence.RollbackException   
	{		
		City city = cityRepository.findOne(id);
			
		if(city != null)
		{
			city.setCity(cityDTO.getCity());
			city.setLastUpdate(new Date());		
			CityDTO ret = new CityDTO(city);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
		
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		City city = cityRepository.findOne(id);
			
		if(city != null)
		{
			cityRepository.delete(city);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	

}
