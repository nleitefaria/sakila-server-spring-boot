package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.FilmTextDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.FilmText;
import com.mycompany.myapp.repository.FilmTextRepository;
import com.mycompany.myapp.service.FilmTextService;

@Service
public class FilmTextServiceImpl implements FilmTextService {

	@Autowired
	FilmTextRepository filmTextRepository;

	@Transactional
	public FilmTextDTO findOne(Short id) {
		FilmText country = filmTextRepository.findOne(id);
		return new FilmTextDTO(country);
	}

	@Transactional
	public List<FilmTextDTO> findAll() {
		List<FilmTextDTO> filmTextDTOList = new ArrayList<FilmTextDTO>();
		List<FilmText> filmTextList = filmTextRepository.findAll();

		for (FilmText filmText : filmTextList) {
			FilmTextDTO filmTextDTO = new FilmTextDTO(filmText);
			filmTextDTOList.add(filmTextDTO);
		}

		return filmTextDTOList;
	}
	
	@Transactional
	public PagginatedDTO<FilmTextDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<FilmTextDTO> filmTextDTOList = new ArrayList<FilmTextDTO>();				
		Page<FilmText> filmTextPage = filmTextRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(FilmText filmText : filmTextPage.getContent())
		{
			FilmTextDTO filmTextDTO = new FilmTextDTO(filmText);
			filmTextDTOList.add(filmTextDTO);		
		}
		
		PagginatedDTO<FilmTextDTO> actorDTOPage = new PagginatedDTO<FilmTextDTO>(filmTextDTOList, filmTextPage.getNumber(), filmTextPage.getNumberOfElements(), filmTextPage.getTotalPages(), filmTextPage.getTotalElements());
		return actorDTOPage;
	}
	
	@Transactional
	public void save(FilmTextDTO filmTextDTO) throws javax.persistence.RollbackException   
	{		
		FilmText filmText = new FilmText(filmTextDTO.getFilmId(), filmTextDTO.getTitle(), filmTextDTO.getDescription());	
		filmTextRepository.save(filmText);	
	}
	
	@Transactional
	public FilmTextDTO update(short id, FilmTextDTO filmTextDTO) throws javax.persistence.RollbackException   
	{		
		FilmText filmText = filmTextRepository.findOne(id);
			
		if(filmText != null)
		{
			filmText.setDescription(filmTextDTO.getDescription());
			FilmTextDTO ret = new FilmTextDTO(filmText);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
		
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		FilmText filmText = filmTextRepository.findOne(id);
			
		if(filmText != null)
		{
			filmTextRepository.delete(filmText);
			return id;
		}
		else
		{
			return null;		
		}			
	}

}
