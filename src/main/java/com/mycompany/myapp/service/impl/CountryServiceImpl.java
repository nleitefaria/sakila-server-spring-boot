package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService
{
	@Autowired
	CountryRepository countryRepository;

	@Transactional
	public CountryDTO findOne(Short id)
	{
		Country country = countryRepository.findOne(id);
		return new CountryDTO(country);
	}

	@Transactional
	public List<CountryDTO> findAll()
	{
		List<CountryDTO> countryDTOList = new ArrayList<CountryDTO>();
		List<Country> countryList = countryRepository.findAll();

		for (Country country : countryList) 
		{
			CountryDTO countryDTO = new CountryDTO(country);
			countryDTOList.add(countryDTO);
		}

		return countryDTOList;
	}
	
	@Transactional
	public PagginatedDTO<CountryDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<CountryDTO> countryDTOList = new ArrayList<CountryDTO>();				
		Page<Country> countryPage = countryRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Country country : countryPage.getContent())
		{
			CountryDTO countryDTO = new CountryDTO(country);
			countryDTOList.add(countryDTO);		
		}
		
		PagginatedDTO<CountryDTO> countryDTOPage = new PagginatedDTO<CountryDTO>(countryDTOList, countryPage.getNumber(), countryPage.getNumberOfElements(), countryPage.getTotalPages(), countryPage.getTotalElements());
		return countryDTOPage;
	}
	
	@Transactional
	public void save(CountryDTO countryDTO) throws javax.persistence.RollbackException
	{									
		Country country = new Country(countryDTO.getCountry(), new Date());
		countryRepository.save(country);	
	}
	
	@Transactional
	public CountryDTO update(short id, CountryDTO countryDTO) throws javax.persistence.RollbackException   
	{		
		Country country = countryRepository.findOne(id);
				
		if(country != null)
		{
			country.setCountry(countryDTO.getCountry());
			country.setLastUpdate(new Date());
			CountryDTO ret = new CountryDTO(country);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
			
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Country country = countryRepository.findOne(id);
				
		if(country != null)
		{
			countryRepository.delete(country);
			return id;
		}
		else
		{
			return null;		
		}			
	}


}
