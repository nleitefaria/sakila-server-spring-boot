package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.FilmDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.repository.FilmRepository;
import com.mycompany.myapp.service.FilmService;

@Service
public class FilmServiceImpl implements FilmService
{
	@Autowired
	FilmRepository filmRepository;

	@Transactional
	public FilmDTO findOne(Short id)
	{
		Film film = filmRepository.findOne(id);
		return new FilmDTO(film);
	}

	@Transactional
	public List<FilmDTO> findAll() 
	{
		List<FilmDTO> filmDTOList = new ArrayList<FilmDTO>();
		List<Film> filmList = filmRepository.findAll();

		for (Film film : filmList) {
			FilmDTO filmDTO = new FilmDTO(film);
			filmDTOList.add(filmDTO);
		}

		return filmDTOList;
	}
	
	@Transactional
	public PagginatedDTO<FilmDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<FilmDTO> filmDTOList = new ArrayList<FilmDTO>();				
		Page<Film> filmPage = filmRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Film film : filmPage.getContent())
		{
			FilmDTO filmDTO = new FilmDTO(film);
			filmDTOList.add(filmDTO);		
		}
		
		PagginatedDTO<FilmDTO> filmDTOPage = new PagginatedDTO<FilmDTO>(filmDTOList, filmPage.getNumber(), filmPage.getNumberOfElements(), filmPage.getTotalPages(), filmPage.getTotalElements());
		return filmDTOPage;
	}
	
	@Transactional
	public void save(FilmDTO filmDTO) throws javax.persistence.RollbackException   
	{
		Film film = new Film(filmDTO.getLanguageByLanguageId() , filmDTO.getTitle() , filmDTO.getRentalDuration(), filmDTO.getRentalRate(), filmDTO.getReplacementCost(), filmDTO.getLastUpdate());	
		filmRepository.save(film);	
	}
	
	@Transactional
	public FilmDTO update(short id, FilmDTO filmDTO) throws javax.persistence.RollbackException   
	{		
		Film film = filmRepository.findOne(id);
		
		if(film != null)
		{
			film.setTitle(filmDTO.getTitle());
			film.setRentalDuration(filmDTO.getRentalDuration());
			FilmDTO ret = new FilmDTO(film);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Film film = filmRepository.findOne(id);
		
		if(film != null)
		{
			filmRepository.delete(film);
			return id;
		}
		else
		{
			return null;		
		}			
	}
}
