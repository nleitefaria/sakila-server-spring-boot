package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Actor;
import com.mycompany.myapp.repository.ActorRepository;
import com.mycompany.myapp.service.ActorService;

@Service
public class ActorServiceImpl implements ActorService
{
	@Autowired
	ActorRepository actorRepository;

	@Transactional
	public ActorDTO findOne(Short id) {
		Actor actor = actorRepository.findOne(id);
		return new ActorDTO(actor);
	}

	@Transactional
	public List<ActorDTO> findAll() {
		List<ActorDTO> actorDTOList = new ArrayList<ActorDTO>();
		List<Actor> actorList = actorRepository.findAll();

		for (Actor actor : actorList) {
			ActorDTO actorDTO = new ActorDTO(actor);
			actorDTOList.add(actorDTO);
		}
		return actorDTOList;
	}
	
	@Transactional
	public PagginatedDTO<ActorDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<ActorDTO> actorDTOList = new ArrayList<ActorDTO>();				
		Page<Actor> actorPage = actorRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Actor actor : actorPage.getContent())
		{
			ActorDTO actorDTO = new ActorDTO(actor);
			actorDTOList.add(actorDTO);		
		}
		
		PagginatedDTO<ActorDTO> actorDTOPage = new PagginatedDTO<ActorDTO>(actorDTOList, actorPage.getNumber(), actorPage.getNumberOfElements(), actorPage.getTotalPages(), actorPage.getTotalElements());
		return actorDTOPage;
	}
	
	@Transactional
	public void save(ActorDTO actorDTO) throws javax.persistence.RollbackException   
	{		
		Actor actor = new Actor(actorDTO.getFirstName(), actorDTO.getLastName(), actorDTO.getLastUpdate());		
		actorRepository.save(actor);	
	}
	
	@Transactional
	public ActorDTO update(short id, ActorDTO actorDTO) throws javax.persistence.RollbackException   
	{		
		Actor actor = actorRepository.findOne(id);
		
		if(actor != null)
		{
			actor.setFirstName(actorDTO.getFirstName());
			actor.setLastName(actorDTO.getLastName());
			actor.setLastUpdate(new Date());
			ActorDTO ret = new ActorDTO(actor);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Actor actor = actorRepository.findOne(id);
		
		if(actor != null)
		{
			actorRepository.delete(actor);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	
	
	

}
