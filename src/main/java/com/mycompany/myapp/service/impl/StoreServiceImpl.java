package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.StoreService;

@Service
public class StoreServiceImpl implements StoreService 
{
	@Autowired
	StoreRepository storeRepository;

	@Transactional
	public StoreDTO findOne(Short id) 
	{
		Store store = storeRepository.findOne(id);
		return new StoreDTO(store);
	}
	
	@Transactional
	public List<StoreDTO> findAll() {
		List<StoreDTO> storeDTOList = new ArrayList<StoreDTO>();
		List<Store> storeList = storeRepository.findAll();

		for (Store store : storeList) {
			StoreDTO storeDTO = new StoreDTO(store);
			storeDTOList.add(storeDTO);
		}

		return storeDTOList;
	}
}
