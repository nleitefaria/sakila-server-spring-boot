package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.InventoryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.entity.Inventory;
import com.mycompany.myapp.entity.Store;

import com.mycompany.myapp.repository.FilmRepository;
import com.mycompany.myapp.repository.InventoryRepository;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.InventoryService;

@Service
public class InventoryServiceImpl implements  InventoryService 
{	
	@Autowired
	InventoryRepository inventoryRepository;
	
	@Autowired
	StoreRepository storeRepository;
	
	@Autowired
	FilmRepository filmRepository;

	@Transactional
	public InventoryDTO findOne(Integer id) 
	{
		Inventory inventory = inventoryRepository.findOne(id);
		return new InventoryDTO(inventory);
	}
	
	@Transactional
	public List<InventoryDTO> findAll() 
	{
		List<InventoryDTO> inventoryDTOList = new ArrayList<InventoryDTO>();
		List<Inventory> inventoryList = inventoryRepository.findAll();

		for (Inventory inventory : inventoryList) 
		{
			InventoryDTO inventoryDTO = new InventoryDTO(inventory);
			inventoryDTOList.add(inventoryDTO);
		}
		return inventoryDTOList;
	}
	
	@Transactional
	public PagginatedDTO<InventoryDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<InventoryDTO> inventoryDTOList = new ArrayList<InventoryDTO>();			
		Page<Inventory> inventoryPage = inventoryRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for (Inventory inventory : inventoryPage.getContent()) {
			InventoryDTO inventoryDTO = new InventoryDTO(inventory);
			inventoryDTOList.add(inventoryDTO);
		}

		
		PagginatedDTO<InventoryDTO> inventoryDTOPage = new PagginatedDTO<InventoryDTO>(inventoryDTOList, inventoryPage.getNumber(), inventoryPage.getNumberOfElements(), inventoryPage.getTotalPages(), inventoryPage.getTotalElements());
		return inventoryDTOPage;
	}
	
	@Transactional
	public void save(InventoryDTO inventoryDTO) throws javax.persistence.RollbackException
	{		
		Film film = filmRepository.findOne(inventoryDTO.getFilm().getFilmId());
		Store store = storeRepository.findOne(inventoryDTO.getStore().getStoreId());
		Inventory inventory = new Inventory(film, store, new Date());
		inventoryRepository.save(inventory);	
	}
	
	

}
