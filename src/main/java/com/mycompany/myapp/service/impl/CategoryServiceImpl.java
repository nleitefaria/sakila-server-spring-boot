package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CategoryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Category;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService
{
	@Autowired
	CategoryRepository categoryRepository;

	@Transactional
	public CategoryDTO findOne(Short id) 
	{
		Category category = categoryRepository.findOne(id);
		return new CategoryDTO(category);
	}
	
	@Transactional
	public List<CategoryDTO> findAll() 
	{
		List<CategoryDTO> categoryDTOList = new ArrayList<CategoryDTO>();
		List<Category> categoryList = categoryRepository.findAll();

		for (Category category : categoryList)
		{
			CategoryDTO categoryDTO = new CategoryDTO(category);
			categoryDTOList.add(categoryDTO);
		}
		
		return categoryDTOList;
	}
	
	@Transactional
	public PagginatedDTO<CategoryDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<CategoryDTO> categoryDTOList = new ArrayList<CategoryDTO>();				
		Page<Category> categoryPage = categoryRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Category category : categoryPage.getContent())
		{
			CategoryDTO categoryDTO = new CategoryDTO(category);
			categoryDTOList.add(categoryDTO);		
		}
		
		PagginatedDTO<CategoryDTO> actorDTOPage = new PagginatedDTO<CategoryDTO>(categoryDTOList, categoryPage.getNumber(), categoryPage.getNumberOfElements(), categoryPage.getTotalPages(), categoryPage.getTotalElements());
		return actorDTOPage;
	}
	
	@Transactional
	public void save(CategoryDTO categoryDTO) throws javax.persistence.RollbackException
	{		
		Category category = new Category(categoryDTO.getName(), new Date());
		categoryRepository.save(category);	
	}
	
	@Transactional
	public CategoryDTO update(short id, CategoryDTO categoryDTO) throws javax.persistence.RollbackException   
	{		
		Category category = categoryRepository.findOne(id);
			
		if(category != null)
		{
			category.setName(categoryDTO.getName());
			category.setLastUpdate(new Date());
			CategoryDTO ret = new CategoryDTO(category);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
				
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Category category = categoryRepository.findOne(id);
			
		if(category != null)
		{
			categoryRepository.delete(category);
			return id;
		}
		else
		{
			return null;		
		}			
	}

}
