package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.RentalDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.entity.Inventory;
import com.mycompany.myapp.entity.Rental;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.repository.FilmRepository;
import com.mycompany.myapp.repository.InventoryRepository;
import com.mycompany.myapp.repository.RentalRepository;
import com.mycompany.myapp.repository.StaffRepository;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.RentalService;

@Service
public class RentalServiceImpl implements RentalService
{
	@Autowired
	RentalRepository rentalRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	InventoryRepository inventoryRepository;
	
	@Autowired
	StaffRepository staffRepository;
	
	@Autowired
	StoreRepository storeRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	FilmRepository filmRepository;

	@Transactional
	public RentalDTO findOne(Integer id) 
	{
		Rental rental = rentalRepository.findOne(id);
		return new RentalDTO(rental);
	}
	
	@Transactional
	public List<RentalDTO> findAll() 
	{
		List<RentalDTO> rentalDTOList = new ArrayList<RentalDTO>();
		List<Rental> rentalList = rentalRepository.findAll();

		for (Rental rental : rentalList)
		{
			RentalDTO rentalDTO = new RentalDTO(rental);
			rentalDTOList.add(rentalDTO);
		}

		return rentalDTOList;
	}
	
	@Transactional
	public PagginatedDTO<RentalDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<RentalDTO> rentalDTOList = new ArrayList<RentalDTO>();				
		Page<Rental> rentalPage = rentalRepository.findAll(new PageRequest(pageNum - 1, pageSize));
			
		for(Rental rental : rentalPage.getContent())
		{
			RentalDTO rentalDTO = new RentalDTO(rental);
			rentalDTOList.add(rentalDTO);		
		}
			
		PagginatedDTO<RentalDTO> rentalDTOPage = new PagginatedDTO<RentalDTO>(rentalDTOList, rentalPage.getNumber(), rentalPage.getNumberOfElements(), rentalPage.getTotalPages(), rentalPage.getTotalElements());
		return rentalDTOPage;
	}
		
	@Transactional
	public void save(RentalDTO rentalDTO) throws javax.persistence.RollbackException
	{
		Address address = addressRepository.findOne(rentalDTO.getCustomer().getAddress().getAddressId());
		Store store = storeRepository.findOne(rentalDTO.getCustomer().getStore().getStoreId());
		Film film = filmRepository.findOne(rentalDTO.getInventory().getFilm().getFilmId());
		
		Customer customer = new Customer(address, store, rentalDTO.getCustomer().getFirstName(), rentalDTO.getCustomer().getLastName(), rentalDTO.getCustomer().isActive(), rentalDTO.getCustomer().getCreateDate(), rentalDTO.getCustomer().getLastUpdate());
		Inventory inventory = new Inventory(film, store, rentalDTO.getInventory().getLastUpdate());
		Staff staff = staffRepository.findOne(rentalDTO.getStaff().getStaffId());
		Rental rental = new Rental(customer, inventory, staff, rentalDTO.getRentalDate(), rentalDTO.getLastUpdate());
		rentalRepository.save(rental);	
	}
	
	
	
	

}
