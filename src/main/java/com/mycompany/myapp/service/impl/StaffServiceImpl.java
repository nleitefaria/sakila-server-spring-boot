package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.StaffRepository;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.StaffService;

@Service
public class StaffServiceImpl implements StaffService
{	
	@Autowired
	StaffRepository staffRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	StoreRepository storeRepository;

	@Transactional
	public StaffDTO findOne(Short id) 
	{
		Staff staff = staffRepository.findOne(id);
		return new StaffDTO(staff);
	}
	
	@Transactional
	public List<StaffDTO> findAll() 
	{
		List<StaffDTO> staffDTOList = new ArrayList<StaffDTO>();
		List<Staff> staffList = staffRepository.findAll();

		for (Staff staff : staffList)
		{
			StaffDTO staffDTO = new StaffDTO(staff);
			staffDTOList.add(staffDTO);
		}

		return staffDTOList;
	}
	
	@Transactional
	public PagginatedDTO<StaffDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<StaffDTO> staffDTOList = new ArrayList<StaffDTO>();			
		Page<Staff> staffPage = staffRepository.findAll(new PageRequest(pageNum - 1, pageSize));
			
		for (Staff staff : staffPage.getContent()) {
			StaffDTO staffDTO = new StaffDTO(staff);
			staffDTOList.add(staffDTO);
		}
		
		PagginatedDTO<StaffDTO> staffDTOPage = new PagginatedDTO<StaffDTO>(staffDTOList, staffPage.getNumber(), staffPage.getNumberOfElements(), staffPage.getTotalPages(), staffPage.getTotalElements());
		return staffDTOPage;
	}
		
	@Transactional
	public void save(StaffDTO staffDTO) throws javax.persistence.RollbackException
	{		
		Address address = addressRepository.findOne(staffDTO.getAddress().getAddressId());
		Store store = storeRepository.findOne(staffDTO.getStore().getStoreId());
		Staff staff = new Staff(address, store, staffDTO.getFirstName(), staffDTO.getLastName(), staffDTO.isActive(), staffDTO.getEmail(), staffDTO.getLastUpdate());		
		staffRepository.save(staff);
	} 
	
	

}
