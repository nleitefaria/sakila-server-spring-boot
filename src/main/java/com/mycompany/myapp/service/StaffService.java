package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.StaffDTO;

public interface StaffService 
{
	StaffDTO findOne(Short id);
	List<StaffDTO> findAll();
	PagginatedDTO<StaffDTO> findAllPaginated(int pageNum, int pageSize);
	void save(StaffDTO staffDTO) throws javax.persistence.RollbackException;
}
