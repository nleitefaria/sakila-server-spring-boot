package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.FilmDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface FilmService 
{	
	FilmDTO findOne(Short id);
	List<FilmDTO> findAll();
	PagginatedDTO<FilmDTO> findAllPaginated(int pageNum, int pageSize);
	void save(FilmDTO filmDTO) throws javax.persistence.RollbackException;   
	FilmDTO update(short id, FilmDTO filmDTO) throws javax.persistence.RollbackException; 
	Short delete(short id) throws javax.persistence.RollbackException;
}
