package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Inventory;


public class InventoryDTO 
{	
	private Integer inventoryId;
	private FilmDTO film;
	private StoreDTO store;
	private Date lastUpdate;
	
	public InventoryDTO(Integer inventoryId, FilmDTO film, StoreDTO store, Date lastUpdate) 
	{
		this.inventoryId = inventoryId;
		this.film = film;
		this.store = store;
		this.lastUpdate = lastUpdate;
	}
	
	public InventoryDTO(Inventory inventory) 
	{
		this.inventoryId = inventory.getInventoryId();
		this.film = new FilmDTO(inventory.getFilm());
		this.store = new StoreDTO(inventory.getStore());
		this.lastUpdate = inventory.getLastUpdate();
	}

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public FilmDTO getFilm() {
		return film;
	}

	public void setFilm(FilmDTO film) {
		this.film = film;
	}

	public StoreDTO getStore() {
		return store;
	}

	public void setStore(StoreDTO store) {
		this.store = store;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
