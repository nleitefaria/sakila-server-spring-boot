package com.mycompany.myapp.domain;

import java.util.Date;
import com.mycompany.myapp.entity.Store;

public class StoreDTO 
{
	private Short storeId;
	private AddressDTO address;
	private StaffDTO staff;
	private Date lastUpdate;
	
	public StoreDTO() 
	{	
	}
	
	public StoreDTO(Short storeId, AddressDTO address, StaffDTO staff, Date lastUpdate) 
	{
		this.storeId = storeId;
		this.address = address;
		this.staff = staff;
		this.lastUpdate = lastUpdate;
	}
	
	public StoreDTO(Store store) 
	{
		this.storeId = store.getStoreId();
		this.address = new AddressDTO(store.getAddress());
		this.staff = new StaffDTO(store.getStaff());
		this.lastUpdate = store.getLastUpdate();
	}

	public Short getStoreId() {
		return storeId;
	}

	public void setStoreId(Short storeId) {
		this.storeId = storeId;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public StaffDTO getStaff() {
		return staff;
	}

	public void setStaff(StaffDTO staff) {
		this.staff = staff;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
