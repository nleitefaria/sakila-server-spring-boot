package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Language;

public class LanguageDTO 
{
	private Short languageId;
	private String name;
	private Date lastUpdate;
	
	public LanguageDTO()
	{
	}
	
	public LanguageDTO(Short languageId, String name, Date lastUpdate)
	{
		this.languageId = languageId;
		this.name = name;
		this.lastUpdate = lastUpdate;
	}
	
	public LanguageDTO(Language language)
	{
		this.languageId = language.getLanguageId();
		this.name = language.getName();
		this.lastUpdate = language.getLastUpdate();
	}
	
	public Short getLanguageId() {
		return languageId;
	}
	
	public void setLanguageId(Short languageId) {
		this.languageId = languageId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
