package com.mycompany.myapp.domain;

import java.util.Date;
import com.mycompany.myapp.entity.Staff;

public class StaffDTO 
{
	private Short staffId;
	private AddressDTO address;
	private StoreDTO store;
	private String firstName;
	private String lastName;
	private byte[] picture;
	private String email;
	private boolean active;
	private String username;
	private String password;
	private Date lastUpdate;
	
	public StaffDTO()
	{
	}
	
	public StaffDTO(Short staffId, AddressDTO address, StoreDTO store, String firstName, String lastName, byte[] picture, String email, boolean active, String username, String password, Date lastUpdate) 
	{
		super();
		this.staffId = staffId;
		this.address = address;
		this.store = store;
		this.firstName = firstName;
		this.lastName = lastName;
		this.picture = picture;
		this.email = email;
		this.active = active;
		this.username = username;
		this.password = password;
		this.lastUpdate = lastUpdate;
	}
	
	public StaffDTO(Staff staff) 
	{
		super();
		this.staffId = staff.getStaffId();
		this.address = new AddressDTO(staff.getAddress());
		this.firstName = staff.getFirstName();
		this.lastName = staff.getLastName();
		this.picture = staff.getPicture();
		this.email = staff.getEmail();
		this.active = staff.isActive();
		this.username = staff.getUsername();
		this.password = staff.getPassword();
		this.lastUpdate = staff.getLastUpdate();
	}

	public Short getStaffId() {
		return staffId;
	}

	public void setStaffId(Short staffId) {
		this.staffId = staffId;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public StoreDTO getStore() {
		return store;
	}

	public void setStore(StoreDTO store) {
		this.store = store;
	}
}
