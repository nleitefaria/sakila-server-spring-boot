package com.mycompany.myapp.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.mycompany.myapp.entity.Payment;
import com.mycompany.myapp.entity.Staff;

public class PaymentDTO
{
	private Short paymentId;
	private CustomerDTO customer;
	private StaffDTO staff;
	private BigDecimal amount;
	private Date paymentDate;
	private Date lastUpdate;
	
	public PaymentDTO()
	{
	}
	
	public PaymentDTO(Short paymentId, CustomerDTO customer, StaffDTO staff, BigDecimal amount, Date paymentDate, Date lastUpdate)
	{
		this.paymentId = paymentId;
		this.customer = customer;
		this.staff = staff;
		this.amount = amount;
		this.paymentDate = paymentDate;
		this.lastUpdate = lastUpdate;
	}
	
	public PaymentDTO(Payment payment)
	{
		this.paymentId = payment.getPaymentId();
		this.customer = new CustomerDTO(payment.getCustomer());
		this.amount = payment.getAmount();
		this.paymentDate = payment.getPaymentDate();
		this.lastUpdate = payment.getLastUpdate();
	}

	public Short getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Short paymentId) {
		this.paymentId = paymentId;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}
	
	

	public StaffDTO getStaff() {
		return staff;
	}

	public void setStaff(StaffDTO staff) {
		this.staff = staff;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
