package com.mycompany.myapp.domain;

import java.util.Date;
import com.mycompany.myapp.entity.City;

public class CityDTO {
	private Short cityId;
	private CountryDTO country;
	private String city;
	private Date lastUpdate;

	public CityDTO() {
	}

	public CityDTO(Short cityId, CountryDTO country, String city, Date lastUpdate) {
		this.cityId = cityId;
		this.country = country;
		this.city = city;
		this.lastUpdate = lastUpdate;
	}

	public CityDTO(City city) {
		this.cityId = city.getCityId();
		this.country = new CountryDTO(city.getCountry());
		this.city = city.getCity();
		this.lastUpdate = city.getLastUpdate();
	}

	public Short getCityId() {
		return cityId;
	}

	public void setCityId(Short cityId) {
		this.cityId = cityId;
	}

	public CountryDTO getCountry() {
		return country;
	}

	public void setCountry(CountryDTO country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
