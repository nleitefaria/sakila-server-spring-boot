package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Actor;

public class ActorDTO {
	private Short actorId;
	private String firstName;
	private String lastName;
	private Date lastUpdate;

	public ActorDTO() {
	}

	public ActorDTO(Short actorId, String firstName, String lastName, Date lastUpdate) {
		this.actorId = actorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastUpdate = lastUpdate;
	}

	public ActorDTO(Actor actor) {
		this.actorId = actor.getActorId();
		this.firstName = actor.getFirstName();
		this.lastName = actor.getLastName();
		this.lastUpdate = actor.getLastUpdate();
	}

	public Short getActorId() {
		return actorId;
	}

	public void setActorId(Short actorId) {
		this.actorId = actorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
