package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Category;

public class CategoryDTO {
	private Short categoryId;
	private String name;
	private Date lastUpdate;

	public CategoryDTO() {
	}

	public CategoryDTO(Category category) {
		this.categoryId = category.getCategoryId();
		this.name = category.getName();
		this.lastUpdate = category.getLastUpdate();
	}

	public CategoryDTO(Short categoryId, String name, Date lastUpdate) {
		this.categoryId = categoryId;
		this.name = name;
		this.lastUpdate = lastUpdate;
	}

	public Short getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
