package com.mycompany.myapp.domain;

import java.util.Date;
import com.mycompany.myapp.entity.Address;

public class AddressDTO 
{
	private Short addressId;
	private CityDTO city;
	private String address;
	private String address2;
	private String district;
	private String postalCode;
	private String phone;
	private Date lastUpdate;

	public AddressDTO() {
	}

	public AddressDTO(Short addressId, CityDTO city, String address, String address2, String district, String postalCode, String phone, Date lastUpdate)
	{
		this.addressId = addressId;
		this.city = city;
		this.address = address;
		this.address2 = address2;
		this.district = district;
		this.postalCode = postalCode;
		this.phone = phone;
		this.lastUpdate = lastUpdate;
	}

	public AddressDTO(Address address) 
	{
		this.addressId = address.getAddressId();
		this.city = new CityDTO(address.getCity());
		this.address = address.getAddress();
		this.address2 = address.getAddress2();
		this.district = address.getDistrict();
		this.postalCode = address.getPostalCode();
		this.phone = address.getPhone();
		this.lastUpdate = address.getLastUpdate();
	}

	public Short getAddressId() {
		return addressId;
	}

	public void setAddressId(Short addressId) {
		this.addressId = addressId;
	}

	public CityDTO getCity() {
		return city;
	}

	public void setCity(CityDTO city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
