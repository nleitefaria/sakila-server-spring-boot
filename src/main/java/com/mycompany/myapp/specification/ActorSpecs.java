package com.mycompany.myapp.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.entity.Actor;
import com.mycompany.myapp.entity.Actor_;
import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.entity.FilmActor;
import com.mycompany.myapp.entity.FilmActor_;
import com.mycompany.myapp.entity.Film_;

public abstract class ActorSpecs 
{
	public static Specifications<Actor> buildSpecification(ActorDTO actorDTO) 
	{
		Specification<Actor> fetchSpec = fetch();
		Specifications<Actor> specs = Specifications.where(fetchSpec);
		
		if(actorDTO.getFirstName() != null)
		{
			if(actorDTO.getFirstName().equals("") == false)
			{
				Specification<Actor> actorFirstNameIsLikeSpec = actorFirstNameIsLike(actorDTO.getFirstName());
				specs = specs.and(actorFirstNameIsLikeSpec);
			}			
		}
		
		if(actorDTO.getLastName() != null)
		{
			if(actorDTO.getLastName().equals("") == false)
			{
				Specification<Actor> actorLastNameIsLikeSpec = actorLastNameIsLike(actorDTO.getLastName());
				specs = specs.and(actorLastNameIsLikeSpec);
			}			
		}
		
		return specs;
	}
	
	private static Specification<Actor> fetch() {
		return new Specification<Actor>() {
			public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				return builder.isNotNull(root.get(Actor_.actorId));
			}
		};
	}
	
	private static Specification<Actor> actorFirstNameIsLike(final String firstName) 
	{
        return new Specification<Actor>() 
        {
          public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder builder) 
          {
             return builder.like(builder.lower(root.get(Actor_.firstName)), firstName.toLowerCase());
          }
        };
    }
	
	private static Specification<Actor> actorLastNameIsLike(final String lastName) 
	{
        return new Specification<Actor>() 
        {
          public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder builder) 
          {
             return builder.like(builder.lower(root.get(Actor_.lastName)), lastName.toLowerCase());
          }
        };
    }
	
	///////////////////////////////////////////
	//Many to Many
	///////////////////////////////////////////
	private static Specification<Actor> actorsByFilm(final Short filmId) 
	{
		return new Specification<Actor>() 
		{		
			public Predicate toPredicate(final Root<Actor> root, final CriteriaQuery<?> query,final CriteriaBuilder cb) {

				final Subquery<Short> classQuery = query.subquery(Short.class);
				final Root<Film> film = classQuery.from(Film.class);
				final Join<Film, FilmActor> filmActor = film.join(Film_.filmActors);
				final Join<FilmActor, Actor> filmActorActor = filmActor.join(FilmActor_.actor);
				classQuery.select(filmActorActor.<Short> get(Actor_.actorId));
				classQuery.where(cb.equal(film.<Short> get(Film_.filmId), filmId));

				return cb.in(root.get(Actor_.actorId)).value(classQuery);
			}
		};
	}
	

}
