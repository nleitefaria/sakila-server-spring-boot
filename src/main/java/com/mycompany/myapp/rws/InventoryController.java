package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.myapp.domain.InventoryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.InventoryService;

@Controller
public class InventoryController 
{
	private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

	@Autowired
	InventoryService inventoryService;

	@RequestMapping(value = "/inventory/{id}", method = RequestMethod.GET)
	public ResponseEntity<InventoryDTO> findOne(@PathVariable Integer id) 
	{
		logger.info("Listing inventory with id: " + id);
		return new ResponseEntity<InventoryDTO>(inventoryService.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/inventory", method = RequestMethod.GET)
	public ResponseEntity<List<InventoryDTO>> findAll() 
	{
		logger.info("Listing all inventories");
		return new ResponseEntity<List<InventoryDTO>>(inventoryService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/inventory/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<InventoryDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing languages");
		return new ResponseEntity<PagginatedDTO<InventoryDTO>>(inventoryService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/inventory", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody InventoryDTO inventoryDTO)
	{       
        logger.info("Creating inventory"); 	
		try
		{
			inventoryService.save(inventoryDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	
	

}
