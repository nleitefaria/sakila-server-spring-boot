package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.service.StoreService;

@Controller
public class StoreController 
{
	private static final Logger logger = LoggerFactory.getLogger(StoreController.class);

	@Autowired
	StoreService storeService;

	@RequestMapping(value = "/store/{id}", method = RequestMethod.GET)
	public ResponseEntity<StoreDTO> findOne(@PathVariable Short id) {
		logger.info("Listing store with id: " + id);
		return new ResponseEntity<StoreDTO>(storeService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/store", method = RequestMethod.GET)
	public ResponseEntity<List<StoreDTO>> findAll() {
		logger.info("Listing all stores");
		return new ResponseEntity<List<StoreDTO>>(storeService.findAll(), HttpStatus.OK);
	}
}
