package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.CountryService;

@RestController
public class CountryResourceController 
{
	private static final Logger logger = LoggerFactory.getLogger(CountryResourceController.class);

	@Autowired
	CountryService countryService;

	@RequestMapping(value = "/country/{id}", method = RequestMethod.GET)
	public ResponseEntity<CountryDTO> findOne(@PathVariable Short id) {
		logger.info("Listing country with id: " + id);
		return new ResponseEntity<CountryDTO>(countryService.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/country", method = RequestMethod.GET)
	public ResponseEntity<List<CountryDTO>> findAll() {
		logger.info("Listing all countries");
		return new ResponseEntity<List<CountryDTO>>(countryService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/country/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<CountryDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing cities");
		return new ResponseEntity<PagginatedDTO<CountryDTO>>(countryService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/country", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody CountryDTO countryDTO)
	{
		logger.info("Creating country " + countryDTO); 	
		try
		{
			countryService.save(countryDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the country");
			logger.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	}
	
	
	@RequestMapping(value = "/country/{id}", method = RequestMethod.PUT)
	public ResponseEntity<CountryDTO> update(@PathVariable Short id, @RequestBody CountryDTO countryDTO) 
	{
		logger.info("Updating country with id: " + id);		
		try
		{
			CountryDTO ret = countryService.update(id, countryDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<CountryDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<CountryDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<CountryDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
		
	@RequestMapping(value = "/country/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
	        logger.info("Deleting entity with id: " + id);	      
	        try
		{
	        	Short ret = countryService.delete(id);
	        	
	        	if(ret != null)
	        	{
	        		logger.info("Done");
	        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
	        	}
	        	else
	        	{
	        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
					return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
	        	}      	
		}
	        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
	}
}
