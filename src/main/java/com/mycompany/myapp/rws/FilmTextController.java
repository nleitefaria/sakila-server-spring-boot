package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.myapp.domain.FilmTextDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.FilmTextService;

@Controller
public class FilmTextController 
{
	private static final Logger logger = LoggerFactory.getLogger(FilmTextController.class);

	@Autowired
	FilmTextService filmTextService;

	@RequestMapping(value = "/filmtext/{id}", method = RequestMethod.GET)
	public ResponseEntity<FilmTextDTO> findOne(@PathVariable Short id) {
		logger.info("Listing film text with id: " + id);
		return new ResponseEntity<FilmTextDTO>(filmTextService.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/filmtext", method = RequestMethod.GET)
	public ResponseEntity<List<FilmTextDTO>> findAll() {
		logger.info("Listing all filmtexts");
		return new ResponseEntity<List<FilmTextDTO>>(filmTextService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filmtext/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<FilmTextDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing filmtexts");
		return new ResponseEntity<PagginatedDTO<FilmTextDTO>>(filmTextService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filmtext", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody FilmTextDTO filmTextDTO)
	{       
        logger.info("Creating filmtext"); 	
		try
		{
			filmTextService.save(filmTextDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	@RequestMapping(value = "/filmtext/{id}", method = RequestMethod.PUT)
	public ResponseEntity<FilmTextDTO> update(@PathVariable Short id, @RequestBody FilmTextDTO filmTextDTO) 
	{
		logger.info("Updating filmtext with id: " + id);		
		try
		{
			FilmTextDTO ret = filmTextService.update(id, filmTextDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<FilmTextDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<FilmTextDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<FilmTextDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
		
	@RequestMapping(value = "/filmtext/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
	        logger.info("Deleting entity with id: " + id);	      
	        try
		{
	        	Short ret = filmTextService.delete(id);
	        	
	        	if(ret != null)
	        	{
	        		logger.info("Done");
	        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
	        	}
	        	else
	        	{
	        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
					return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
	        	}      	
		}
	        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
	}
}
