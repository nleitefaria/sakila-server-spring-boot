package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.PaymentDTO;
import com.mycompany.myapp.service.PaymentService;

@RestController
public class PaymentController
{
	private static final Logger logger = LoggerFactory.getLogger(ActorResourceController.class);

	@Autowired
	PaymentService paymentService;

	@RequestMapping(value = "/payment/{id}", method = RequestMethod.GET)
	public ResponseEntity<PaymentDTO> findOne(@PathVariable Short id)
	{
		logger.info("Listing payment with id: " + id);
		return new ResponseEntity<PaymentDTO>(paymentService.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public ResponseEntity<List<PaymentDTO>> findAll() 
	{
		logger.info("Listing all payments");
		return new ResponseEntity<List<PaymentDTO>>(paymentService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/payment/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<PaymentDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing payments");
		return new ResponseEntity<PagginatedDTO<PaymentDTO>>(paymentService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
		
	@RequestMapping(value = "/payment", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody PaymentDTO paymentDTO)
	{
		logger.info("Creating payment " + paymentDTO); 	
		try
		{
			paymentService.save(paymentDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the payment");
			logger.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	}
}
