package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.service.StaffService;

@Controller
public class StaffController
{	
	private static final Logger logger = LoggerFactory.getLogger(StaffController.class);

	@Autowired
	StaffService staffService;

	@RequestMapping(value = "/staff/{id}", method = RequestMethod.GET)
	public ResponseEntity<StaffDTO> findOne(@PathVariable Short id) {
		logger.info("Listing store with id: " + id);
		return new ResponseEntity<StaffDTO>(staffService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/staff", method = RequestMethod.GET)
	public ResponseEntity<List<StaffDTO>> findAll() {
		logger.info("Listing all stores");
		return new ResponseEntity<List<StaffDTO>>(staffService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/staff/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<StaffDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing staffs");
		return new ResponseEntity<PagginatedDTO<StaffDTO>>(staffService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
		
	@RequestMapping(value = "/staff", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody StaffDTO staffDTO)
	{
		logger.info("Creating staff " + staffDTO); 	
		try
		{
			staffService.save(staffDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the staff");
			logger.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	}
	
	

}
