package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.CityService;

@RestController
public class CityResourceController
{
	private static final Logger logger = LoggerFactory.getLogger(CityResourceController.class);

	@Autowired
	CityService cityService;

	@RequestMapping(value = "/city/{id}", method = RequestMethod.GET)
	public ResponseEntity<CityDTO> findOne(@PathVariable Short id) {
		logger.info("Listing address with id: " + id);
		return new ResponseEntity<CityDTO>(cityService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/city", method = RequestMethod.GET)
	public ResponseEntity<List<CityDTO>> findAll()
	{
		logger.info("Listing all cities");
		return new ResponseEntity<List<CityDTO>>(cityService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/city/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<CityDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing cities");
		return new ResponseEntity<PagginatedDTO<CityDTO>>(cityService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/city", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody CityDTO cityDTO)
	{
		logger.info("Creating city " + cityDTO); 	
		try
		{
			cityService.save(cityDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the city");
			logger.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	}
	
	@RequestMapping(value = "/city/{id}", method = RequestMethod.PUT)
	public ResponseEntity<CityDTO> update(@PathVariable Short id, @RequestBody CityDTO cityDTO) 
	{
		logger.info("Updating city with id: " + id);		
		try
		{
			CityDTO ret = cityService.update(id, cityDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<CityDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<CityDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the category");
			return new ResponseEntity<CityDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
		
	@RequestMapping(value = "/city/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
	        logger.info("Deleting entity with id: " + id);	      
	        try
		{
	        	Short ret = cityService.delete(id);
	        	
	        	if(ret != null)
	        	{
	        		logger.info("Done");
	        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
	        	}
	        	else
	        	{
	        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
					return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
	        	}      	
		}
	        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
	}

}
