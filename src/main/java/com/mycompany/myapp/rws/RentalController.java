package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.RentalDTO;
import com.mycompany.myapp.service.RentalService;

@RestController
public class RentalController 
{	
	private static final Logger logger = LoggerFactory.getLogger(RentalController.class);

	@Autowired
	RentalService rentalService;

	@RequestMapping(value = "/rental/{id}", method = RequestMethod.GET)
	public ResponseEntity<RentalDTO> findOne(@PathVariable Integer id) {
		logger.info("Listing rental with id: " + id);
		return new ResponseEntity<RentalDTO>(rentalService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rental", method = RequestMethod.GET)
	public ResponseEntity<List<RentalDTO>> findAll() {
		logger.info("Listing all rentals");
		return new ResponseEntity<List<RentalDTO>>(rentalService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rental/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<RentalDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing rentals");
		return new ResponseEntity<PagginatedDTO<RentalDTO>>(rentalService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rental", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody RentalDTO rentalDTO)
	{
		logger.info("Creating rental " + rentalDTO); 	
		try
		{
			rentalService.save(rentalDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the rental");
			logger.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	}
	
	

}
