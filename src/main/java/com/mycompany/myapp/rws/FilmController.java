package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mycompany.myapp.domain.FilmDTO;
import com.mycompany.myapp.service.FilmService;

@Controller
public class FilmController 
{
	private static final Logger logger = LoggerFactory.getLogger(FilmController.class);

	@Autowired
	FilmService filmService;

	@RequestMapping(value = "/film/{id}", method = RequestMethod.GET)
	public ResponseEntity<FilmDTO> findOne(@PathVariable Short id) {
		logger.info("Listing film with id: " + id);
		return new ResponseEntity<FilmDTO>(filmService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/film", method = RequestMethod.GET)
	public ResponseEntity<List<FilmDTO>> findAll() {
		logger.info("Listing all customers");
		return new ResponseEntity<List<FilmDTO>>(filmService.findAll(), HttpStatus.OK);
	}

}
