package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.AddressService;

@RestController
public class AddressResourceController 
{
	private static final Logger logger = LoggerFactory.getLogger(AddressResourceController.class);

	@Autowired
	AddressService addressService;

	@RequestMapping(value = "/address/{id}", method = RequestMethod.GET)
	public ResponseEntity<AddressDTO> findOne(@PathVariable Short id)
	{
		logger.info("Listing address with id: " + id);
		return new ResponseEntity<AddressDTO>(addressService.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/address", method = RequestMethod.GET)
	public ResponseEntity<List<AddressDTO>> findAll()
	{
		logger.info("Listing all addresses");
		return new ResponseEntity<List<AddressDTO>>(addressService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/address/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<AddressDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing addresses");
		return new ResponseEntity<PagginatedDTO<AddressDTO>>(addressService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/address", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody AddressDTO addressDTO)
	{
		logger.info("Creating address " + addressDTO); 	
		try
		{
			addressService.save(addressDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the address");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	}
	
	@RequestMapping(value = "/address/{id}", method = RequestMethod.PUT)
	public ResponseEntity<AddressDTO> update(@PathVariable Short id, @RequestBody AddressDTO addressDTO) 
	{
		logger.info("Updating address with id: " + id);		
		try
		{
			AddressDTO ret = addressService.update(id, addressDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<AddressDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<AddressDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<AddressDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/address/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Short ret = addressService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
    }
	
}
