package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Staff.class)
public abstract class Staff_ {

	public static volatile SingularAttribute<Staff, String> lastName;
	public static volatile SingularAttribute<Staff, Short> staffId;
	public static volatile SingularAttribute<Staff, Store> store;
	public static volatile SetAttribute<Staff, Rental> rentals;
	public static volatile SingularAttribute<Staff, Date> lastUpdate;
	public static volatile SingularAttribute<Staff, String> password;
	public static volatile SingularAttribute<Staff, byte[]> picture;
	public static volatile SingularAttribute<Staff, String> username;
	public static volatile SingularAttribute<Staff, Address> address;
	public static volatile SingularAttribute<Staff, String> email;
	public static volatile SetAttribute<Staff, Payment> payments;
	public static volatile SingularAttribute<Staff, Boolean> active;
	public static volatile SetAttribute<Staff, Store> stores;
	public static volatile SingularAttribute<Staff, String> firstName;

}
