package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Address.class)
public abstract class Address_ {

	public static volatile SingularAttribute<Address, String> phone;
	public static volatile SingularAttribute<Address, String> postalCode;
	public static volatile SingularAttribute<Address, String> address;
	public static volatile SingularAttribute<Address, Date> lastUpdate;
	public static volatile SingularAttribute<Address, String> address2;
	public static volatile SetAttribute<Address, Staff> staffs;
	public static volatile SetAttribute<Address, Store> stores;
	public static volatile SetAttribute<Address, Customer> customers;
	public static volatile SingularAttribute<Address, String> district;
	public static volatile SingularAttribute<Address, Short> addressId;
	public static volatile SingularAttribute<Address, City> city;

}
