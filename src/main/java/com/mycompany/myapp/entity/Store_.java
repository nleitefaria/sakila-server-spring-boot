package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Store.class)
public abstract class Store_ {

	public static volatile SingularAttribute<Store, Staff> staff;
	public static volatile SetAttribute<Store, Inventory> inventories;
	public static volatile SingularAttribute<Store, Address> address;
	public static volatile SingularAttribute<Store, Date> lastUpdate;
	public static volatile SetAttribute<Store, Staff> staffs;
	public static volatile SetAttribute<Store, Customer> customers;
	public static volatile SingularAttribute<Store, Short> storeId;

}
