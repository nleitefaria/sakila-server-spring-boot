package com.mycompany.myapp.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Payment.class)
public abstract class Payment_ {

	public static volatile SingularAttribute<Payment, BigDecimal> amount;
	public static volatile SingularAttribute<Payment, Date> paymentDate;
	public static volatile SingularAttribute<Payment, Staff> staff;
	public static volatile SingularAttribute<Payment, Date> lastUpdate;
	public static volatile SingularAttribute<Payment, Short> paymentId;
	public static volatile SingularAttribute<Payment, Rental> rental;
	public static volatile SingularAttribute<Payment, Customer> customer;

}
