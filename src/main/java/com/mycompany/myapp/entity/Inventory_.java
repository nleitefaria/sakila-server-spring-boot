package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Inventory.class)
public abstract class Inventory_ {

	public static volatile SingularAttribute<Inventory, Film> film;
	public static volatile SingularAttribute<Inventory, Store> store;
	public static volatile SetAttribute<Inventory, Rental> rentals;
	public static volatile SingularAttribute<Inventory, Date> lastUpdate;
	public static volatile SingularAttribute<Inventory, Integer> inventoryId;

}
