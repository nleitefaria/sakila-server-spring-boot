package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Language.class)
public abstract class Language_ {

	public static volatile SetAttribute<Language, Film> filmsForOriginalLanguageId;
	public static volatile SingularAttribute<Language, Date> lastUpdate;
	public static volatile SingularAttribute<Language, String> name;
	public static volatile SetAttribute<Language, Film> filmsForLanguageId;
	public static volatile SingularAttribute<Language, Short> languageId;

}
