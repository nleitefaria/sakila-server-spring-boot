package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Rental.class)
public abstract class Rental_ {

	public static volatile SingularAttribute<Rental, Staff> staff;
	public static volatile SingularAttribute<Rental, Date> lastUpdate;
	public static volatile SingularAttribute<Rental, Inventory> inventory;
	public static volatile SetAttribute<Rental, Payment> payments;
	public static volatile SingularAttribute<Rental, Integer> rentalId;
	public static volatile SingularAttribute<Rental, Customer> customer;
	public static volatile SingularAttribute<Rental, Date> returnDate;
	public static volatile SingularAttribute<Rental, Date> rentalDate;

}
