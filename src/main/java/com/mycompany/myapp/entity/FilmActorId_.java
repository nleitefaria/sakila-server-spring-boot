package com.mycompany.myapp.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(FilmActorId.class)
public abstract class FilmActorId_ {

	public static volatile SingularAttribute<FilmActorId, Short> actorId;
	public static volatile SingularAttribute<FilmActorId, Integer> hashCode;
	public static volatile SingularAttribute<FilmActorId, Short> filmId;

}
