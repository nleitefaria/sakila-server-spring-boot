package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(FilmActor.class)
public abstract class FilmActor_ {

	public static volatile SingularAttribute<FilmActor, Film> film;
	public static volatile SingularAttribute<FilmActor, FilmActorId> id;
	public static volatile SingularAttribute<FilmActor, Date> lastUpdate;
	public static volatile SingularAttribute<FilmActor, Actor> actor;

}
