package com.mycompany.myapp.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(FilmText.class)
public abstract class FilmText_ {

	public static volatile SingularAttribute<FilmText, Short> filmId;
	public static volatile SingularAttribute<FilmText, String> description;
	public static volatile SingularAttribute<FilmText, String> title;

}
