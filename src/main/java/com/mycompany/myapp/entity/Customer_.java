package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Customer.class)
public abstract class Customer_ {

	public static volatile SingularAttribute<Customer, String> lastName;
	public static volatile SingularAttribute<Customer, Short> customerId;
	public static volatile SingularAttribute<Customer, Store> store;
	public static volatile SetAttribute<Customer, Rental> rentals;
	public static volatile SingularAttribute<Customer, String> email;
	public static volatile SingularAttribute<Customer, Address> address;
	public static volatile SingularAttribute<Customer, Date> lastUpdate;
	public static volatile SetAttribute<Customer, Payment> payments;
	public static volatile SingularAttribute<Customer, Boolean> active;
	public static volatile SingularAttribute<Customer, Date> createDate;
	public static volatile SingularAttribute<Customer, String> firstName;

}
