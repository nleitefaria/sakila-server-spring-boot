package com.mycompany.myapp.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Film.class)
public abstract class Film_ {

	public static volatile SingularAttribute<Film, String> specialFeatures;
	public static volatile SingularAttribute<Film, Date> lastUpdate;
	public static volatile SingularAttribute<Film, Language> languageByOriginalLanguageId;
	public static volatile SingularAttribute<Film, Byte> rentalDuration;
	public static volatile SingularAttribute<Film, Date> releaseYear;
	public static volatile SingularAttribute<Film, String> title;
	public static volatile SetAttribute<Film, Inventory> inventories;
	public static volatile SingularAttribute<Film, String> description;
	public static volatile SingularAttribute<Film, BigDecimal> replacementCost;
	public static volatile SingularAttribute<Film, Short> length;
	public static volatile SetAttribute<Film, FilmCategory> filmCategories;
	public static volatile SingularAttribute<Film, String> rating;
	public static volatile SingularAttribute<Film, BigDecimal> rentalRate;
	public static volatile SetAttribute<Film, FilmActor> filmActors;
	public static volatile SingularAttribute<Film, Language> languageByLanguageId;
	public static volatile SingularAttribute<Film, Short> filmId;

}
