package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Short>, JpaSpecificationExecutor<Category> {

}
