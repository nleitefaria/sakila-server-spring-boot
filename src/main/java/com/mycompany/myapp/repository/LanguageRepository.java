package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.mycompany.myapp.entity.Language;

public interface LanguageRepository extends JpaRepository<Language, Short>, JpaSpecificationExecutor<Language> {

}
