package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Film;

public interface FilmRepository extends JpaRepository<Film, Short>, JpaSpecificationExecutor<Film> {
}
