package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Short>, JpaSpecificationExecutor<Payment> {

}
