package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.FilmText;

public interface FilmTextRepository extends JpaRepository<FilmText, Short>, JpaSpecificationExecutor<FilmText> {

}
