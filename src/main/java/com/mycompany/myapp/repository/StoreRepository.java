package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Store;

public interface StoreRepository extends JpaRepository<Store, Short>, JpaSpecificationExecutor<Store> {

}
