package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.mycompany.myapp.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Short>, JpaSpecificationExecutor<Customer> {
	@Query("SELECT c FROM Customer c LEFT JOIN FETCH c.address")
	List<Customer> findAllCustomers();
}
