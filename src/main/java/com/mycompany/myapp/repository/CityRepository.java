package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import com.mycompany.myapp.entity.City;

public interface CityRepository extends JpaRepository<City, Short>, JpaSpecificationExecutor<City> {

	@Query("SELECT c FROM City c LEFT JOIN FETCH c.country")
	List<City> findAllCities();

}
